def convert(val, input, output):
    if input == "Kelvin":
        if output == "Celsius":
            return val-273.15
        elif output == "Fahrenheit":
            return (val-273.15)*1.8000+32
        else:
            return "Ошибка! Введена не верная шкала"
    elif input == "Celsius":
        if output == "Kelvin":
            return val+273.15
        elif output == "Fahrenheit":
            return (val*1.8000)+32
        else:
            return "Ошибка! Введена не верная шкала"
    elif input == "Fahrenheit":
        if output == "Celsius":
            return (val - 32) * 5/9
        elif output == "Kalvin":
            return (val - 32) * 5/9 + 273.15
        else:
            return "Ошибка! Введена не верная шкала"
    else:
        return "Ошибка! Введена не верная шкала"

